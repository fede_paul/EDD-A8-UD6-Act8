package com.edd.test;

public class SensorStub extends Sensor{

    private double pressure;

    SensorStub(double pressure ) {

        this.pressure = pressure;

    }

    public double popNextPressurePsiValue() {

        return this.pressure;

    }

}
