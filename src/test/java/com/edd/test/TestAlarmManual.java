package com.edd.test;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

class TestAlarmManual {

    /**
     * Comprobar que la alarma está desactivada por defecto
     */
    @Test
    void testAlarmIsNotOnByDefault() {

        Sensor sensor = new Sensor();
        Alarm alarm = new Alarm(sensor);

        assertFalse(alarm.isAlarmOn());

    }
    /**
     * Comprobar que la alarma se activa en con bajas presiones
     */
    @Test
    void testAlarmOnWithLowPressure() {

        Sensor sensor = new SensorStub(15);
        Alarm alarm = new Alarm(sensor);

        alarm.check();

        assertTrue(alarm.isAlarmOn());

    }

    /**
     * Comprobar que la alarma se activa en con altas presiones
     */
    @Test
    void testAlarmOnWithHighPressure() {


        Sensor sensor = new SensorStub(22);
        Alarm alarm = new Alarm(sensor);

        alarm.check();

        assertTrue(alarm.isAlarmOn());

    }

    /**
     * Comprobar que la alarma permanece desactivada con presiones normales
     */
    @ParameterizedTest
    @MethodSource("safeRange")
    void testAlarmOffWithNormalPressure( double pressure) {

        Sensor sensor = new SensorStub(pressure);
        Alarm alarm = new Alarm(sensor);

        alarm.check();

        assertFalse(alarm.isAlarmOn());

    }

    private static DoubleStream safeRange() {

        return IntStream.range(17, 21).asDoubleStream();

    }

    /**
     * Comprobar que la alarma permanece desactivada con presiones límite
     */
    @ParameterizedTest
    @CsvSource({"17","21"})
    void testAlarmOffWithLimitsPressure(double pressure) {

        Sensor sensor = new SensorStub(pressure);
        Alarm alarm = new Alarm(sensor);

        alarm.check();

        assertFalse(alarm.isAlarmOn());

    }

    /**
     * Comprobar que la alarma se activa con presiones límite por encima y por debajo
     */
    @ParameterizedTest
    @CsvSource({"16","22"})
    void testAlarmOnWithLimitsPressure( double pressure ) {

        Sensor sensor = new SensorStub(pressure);
        Alarm alarm = new Alarm(sensor);

        alarm.check();

        assertTrue(alarm.isAlarmOn());

    }

}
