package com.edd.test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestAlarmMockito {

    private Sensor sensor;

    @BeforeEach
    void testBench () {

        this.sensor = mock(Sensor.class);

    }

    /**
     * Comprobar que la alarma está desactivada por defecto
     */
    @Test
    void testAlarmIsNotOnByDefault() {

        Sensor sensor = new Sensor();
        Alarm alarm = new Alarm(sensor);

        assertFalse(alarm.isAlarmOn());

    }
    /**
     * Comprobar que la alarma se activa en con bajas presiones
     */
    @Test
    void testAlarmOnWithLowPressure() {

        when(this.sensor.popNextPressurePsiValue()).thenReturn(15d);
        Alarm alarm = new Alarm(this.sensor);

        alarm.check();

        assertTrue(alarm.isAlarmOn());

    }

    /**
     * Comprobar que la alarma se activa en con altas presiones
     */
    @Test
    void testAlarmOnWithHighPressure() {

        when(this.sensor.popNextPressurePsiValue()).thenReturn(22d);
        Alarm alarm = new Alarm(sensor);

        alarm.check();

        assertTrue(alarm.isAlarmOn());

    }

    /**
     * Comprobar que la alarma permanece desactivada con presiones normales
     */
    @ParameterizedTest
    @MethodSource("safeRange")
    void testAlarmOffWithNormalPressure( double pressure) {

        when(this.sensor.popNextPressurePsiValue()).thenReturn(pressure);
        Alarm alarm = new Alarm(sensor);

        alarm.check();

        assertFalse(alarm.isAlarmOn());

    }

    private static DoubleStream safeRange() {

        return IntStream.range(17, 21).asDoubleStream();

    }

    /**
     * Comprobar que la alarma permanece desactivada con presiones límite
     */
    @ParameterizedTest
    @CsvSource({"17","21"})
    void testAlarmOffWithLimitsPressure(double pressure) {

        when(this.sensor.popNextPressurePsiValue()).thenReturn(pressure);
        Alarm alarm = new Alarm(sensor);

        alarm.check();

        assertFalse(alarm.isAlarmOn());

    }

    /**
     * Comprobar que la alarma se activa con presiones límite por encima y por debajo
     */
    @ParameterizedTest
    @CsvSource({"16","22"})
    void testAlarmOnWithLimitsPressure( double pressure ) {

        when(this.sensor.popNextPressurePsiValue()).thenReturn(pressure);
        Alarm alarm = new Alarm(sensor);

        alarm.check();

        assertTrue(alarm.isAlarmOn());

    }

}
